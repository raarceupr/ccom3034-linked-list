#include "Llist.h"

void LList::insert(ElementType val, int pos) {
    
    // Validate position
    if ( (pos < 0) || (pos > mySize) ) { 
        cerr << "Attempting insert at illegal position " << pos << endl;
        exit(1);
    }
    
    // create the new node to insert
    Node *n = new Node(val);
    
    if (pos == 0) { 
        // inserting at the first position
        n->next = first;
        first = n;
    } else {
        Node *curr = first;
        // move to the position
        for (int i=1; i<pos; i++) curr = curr->next;  
        n->next = curr->next;
        curr->next = n;
    }
    mySize++;    
}
    
void LList::erase(int pos) {

    // Validate position
    if ( (pos < 0) || (pos > mySize - 1) ) { 
        cerr << "Attempting delete at illegal position " << pos << endl;
        exit(1);
    }
    
    Node* tmp;
    if (pos == 0) { 
        // erasing the node at the first position
        tmp = first;
        first = first->next;
    } else {
        Node *curr = first;
        // move to the position before the node to be erased
        for (int i=1; i<pos; i++)   curr = curr->next;  
        tmp = curr->next;
        curr->next = curr->next->next;
    }
    delete tmp;

    mySize--; 
}
    
void LList::display(ostream &out) const {
    Node *curr = first;
    while (curr != NULL) {
        out << curr->data << " ";
        curr = curr->next;
    }
    //out << "\n";
}

/*
   sort(): Implemented using selection sort.
      t: pointer to the element considered for swaping.
      min: pointer to minimum element found in this iteration
      moving: pointer to explore the complete list.
          
   For example, at the end of the first outer iteration before the 
   swaping, the state of the pointers is as follows.

      9, 4, 5, 1, 8
      ^        ^  ^
      t        m  moving
*/

void LList::sort() {
  Node* t = first;
  Node* min;
  Node* moving;

  while(t!=NULL) {
    min = t; 
    moving = t->next;
    while(moving!=NULL) {
      if (moving->data < min->data) min = moving;
      moving = moving->next;
    }
    swap(t->data, min->data);
    t = t->next; 
  }
}

bool LList::operator==(const LList& L) const {
    if (mySize != L.mySize) return false; 
    Node *p = first;
    Node *q = L.first;
    while (p != NULL) {
        if (p->data != q->data) return false;
        p = p->next;
        q = q->next;
    }
    return true;
}



void LList::push(ElementType e) {
    cout << "inserting " << e << " at pos: " << mySize << endl;
    insert(e,mySize);
}

ostream & operator<< (ostream &out, const LList & L) {
    L.display(out);
    return out;
}

