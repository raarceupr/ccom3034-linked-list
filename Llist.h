#ifndef LLIST_H
#define LLIST_H

#include <iostream>

using namespace std;

typedef int ElementType;


class Node {
private:
    ElementType data;
    Node* next;
public:

    // Constructor, can call with 0, 1, or 2 params.
    Node(int d = 0, Node* n = NULL) : data(d), next(n)  {}

    // display with address for class illustration purposeds
    void display_wadd(ostream& out) {
        out << dec << data << "{" << hex << this << "}";
    }

// Make Llist a friend so that we can access all Node objects info.
friend class LList;
};

class LList {
private:
    Node* first;
    int   mySize;
public:
    // Constructor.
    LList(): first(NULL), mySize(0) {}
    
    void insert(ElementType val, int pos);
    
    // erase the node at position pos.
    void erase(int pos); 
    
    void display(ostream &out) const;

    void sort();
    void push(ElementType e);

    bool operator==(const LList& L) const;

};

ostream & operator<< (ostream &, const LList &);

#endif
