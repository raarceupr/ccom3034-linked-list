CC=g++
DEPS = Llist.h
OBJ = LlistClient.o Llist.o 
SOURCE = LlistClient.cpp Llist.cpp 

all: LlistClient

LlistClient: $(OBJ)
	$(CC) -o $@ $^ 

%.o: %.cpp $(DEPS)
	$(CC) -c $< 

pack: 
	tar cvzf Llist.tgz $(SOURCE) $(DEPS) Makefile

clean:
	rm -Rf *.o LlistClient
