#include "Llist.h"
#include <iostream>
#include <cassert>
using namespace std;

const int TEST_SIZE = 10;
/*
void testInsertInOrder() {
  LList L1, L2;
  int r;

  for (int i=0; i<TEST_SIZE; i++) {
    r = rand() % 20;
    L1.insert(r,0);
    L2.insert(r,0);
  }
  r = rand() % 20;
  L2.insert(r,0);
  L1.sort();
  L2.sort();
  
  cout << L2 << endl;

  cout << "L1 before insert in order: " << L1 << endl;
  L1.insertInOrder(r);
  cout << "L1 after  insert in order: " << L1 << endl;

  assert(L1 == L2);
  cout << "Passed the testInsertInOrder." << endl; 
}
*/

/*
void testInsertList() {
    int a, b, r;
    LList L1, L2, L3;
    a = rand() % 10;
    b = a + rand() % 10;
    for (int i = 0; i < 20; i++) {
       r = rand() % 20;
       if (i >= a && i <= b) 
           L2.push(r);
       else 
           L1.push(r);
       L3.push(r);
    } 
  cout << "L1: " << L1 << endl;
  cout << "L2: " << L2 << endl;
  cout << "L3: " << L3 << endl;
  L1.insert(L2,a);
  cout << "L1: " << L1 << endl;
  assert(L3 == L1);
}

/*
void testElimDuplicates() {
    int A[] = {1, 2, 3, 4, 5};
    LList L1, L2;
    for (int i=0; i<5; i++) {
        L1.push(A[i]);
        L2.push(A[i]);
        for (int j=0; j < rand()%5; j++) {
            L2.push(A[rand()%5]);
        }
    }
    cout << "L1:" << L1 << endl;
    cout << "L2:" << L2 << endl;
    L2.elimDuplicates();
    cout << "After Elim Dups L2:" << L2 << endl;
    L2.sort();
    assert(L1==L2);
    
}
*/

int main() {
    srand(time(NULL));

    cout << "I am the client, feed me." << endl;
    //testInsertInOrder();
    //testInsertList(); 
    //testElimDuplicates();
    
}
